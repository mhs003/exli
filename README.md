exli
====
`exli` extracts links from pdf or txt files.

## install

in **Termux**:

clone the repo:
```bash
git clone https://gitlab.com/mhs003/exli.git
cd exli 
```

install requirements:
```bash
pkg install python
pip install PyPDF2
```

change installer file's permission:
```bash
chmod +x install-termux.bash
```

install `exli`:
```bash
. install-termux.bash
```

---

in **Linux**:

clone the repo:
```bash
git clone https://gitlab.com/mhs003/exli.git
cd exli 
```

install requirements:
```bash
sudo apt install python3
pip3 install PyPDF2
```

change installer file's permission:
```bash
chmod +x install-linux.bash
```

install `exli`:
```bash
. install-linux.bash
```


## usage

use the command to view help document:
```bash
exli -h
```