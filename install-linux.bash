#!/bin/bash

if [ "$(id -u)" -ne 0 ]; then
  echo 'Please run with sudo'
  exit
fi

echo -e "\e[1;34mInstalling...\e[0m"
cp linux.exli.py /usr/bin/exli

chmod +x /usr/bin/exli

echo -e "\e[1;32mInstallation finished!\e[0m"