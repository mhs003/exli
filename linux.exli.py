#!/bin/python3

import PyPDF2
import sys
import re

## Functions
def findUrls(string):
  regex = r"\b((?:https?://)?(?:(?:www\.)?(?:[\da-z\.-]+)\.(?:[a-z]{2,6})|(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)|(?:(?:[0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,7}:|(?:[0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,5}(?::[0-9a-fA-F]{1,4}){1,2}|(?:[0-9a-fA-F]{1,4}:){1,4}(?::[0-9a-fA-F]{1,4}){1,3}|(?:[0-9a-fA-F]{1,4}:){1,3}(?::[0-9a-fA-F]{1,4}){1,4}|(?:[0-9a-fA-F]{1,4}:){1,2}(?::[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:(?:(?::[0-9a-fA-F]{1,4}){1,6})|:(?:(?::[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(?::[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(?:ffff(?::0{1,4}){0,1}:){0,1}(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])|(?:[0-9a-fA-F]{1,4}:){1,4}:(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])))(?::[0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])?(?:/[\w\%\:\.-]*)*/?)\b"
  
  urls = re.findall(regex, string)
  return urls

def getFromTxtf(fname):
  txtLinks = []
  txtf = open(fname, 'r')
  return findUrls(txtf.read())

def getFromPdf(fname):
  pdfLinks = []
  PDFFile = open(fname, 'rb')
  PDF = PyPDF2.PdfReader(PDFFile)
  pages = len(PDF.pages)
  key = '/Annots'
  uri = '/URI'
  ank = '/A'

  for page in range(pages):
    pageSliced = PDF.pages[page]
    ## Text links
    pdfText = pageSliced.extract_text()
    for textLinks in findUrls(pdfText):
      pdfLinks.append(textLinks)
  
    ## Hyper links
    pageObject = pageSliced.get_object()
    if key in pageObject.keys():
      ann = pageObject[key]
      for a in ann:
        u = a.get_object()
        if uri in u[ank].keys():
          pdfLinks.append(u[ank][uri])
  
  return pdfLinks
  
def printList(alist):
  for itm in alist:
    print(itm)
  
def printHelp():
  print("""
Usage: exli [options] [filename.ext]

  Extract all links from a pdf file or any txt file.
  By default it will extract links from txt file. Use '-p' flag to extract from pdf files.

Options:
  -p               Extract links from a pdf file
  -v, --version    Print program version
  -h, --help       Print this help page

Example usage:
  exli file.txt
  exli -p file.pdf
  
Version: v1.0
  """.strip())
  
  exit()
  
  

## -----------------
## Main Program

args = sys.argv
pdf = False

if len(args) > 1:
  filename = args[1]
else:
  printHelp()


if '-h' in args or '--help' in args:
  printHelp()
elif '-v' in args or '--version' in args:
  print('v1.0')
  exit()
elif '-p' in args:
  args.remove('-p')
  if len(args) > 1:
    filename = args[1]
    pdf = True
  else:
    printHelp()
elif args[1].startswith('-'):
  args.pop(1)
  if len(args) > 1:
    filename = args[1]
  else:
    printHelp()

if pdf == True:
  printList(getFromPdf(filename))
else:
  printList(getFromTxtf(filename))